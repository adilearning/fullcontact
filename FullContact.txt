FullContact
------------

-1. Authentication-
	The primary and recommended method for authenticating with FullContact is to specify the API key 
	in the HTTP request header using an extended header field with the name X-FullContact-APIKey.
	The alterntive is: specifying the API key in the query parameter in the form of apiKey=.
	=> HTTP request header is better becouse its provides an added level of security.
	Although we utilize HTTPS to ensure that all requests are encrypted for network transport, there is a possibility that:
	- The plain-text URI, with the value of the apiKey, might appear in logs of HTTP servers which process the requests.
	- There are spyware that browser extensions track and aggregate browsing behavior and sell that data to third parties, apiKey as a query parameter could lend 
	itself to unintentional exposure of your API key.

2. Calls to the API- 
	asynchronous or synchronous: If an API call is synchronous, it means that code execution will block (or wait) for the API call to return before 
	continuing. This means that until a response is returned by the API, your application will not execute any further, which could be perceived by the
	user as latency or performance lag in your app, but if we will have 2 duplicate emails one after the other, we won't send the secound one => so 
	it might improves performance in this case.
	On the other hand, if the file is very big, we can read the file on blanks of 50 or so, in each treade and to do it on parallel.

3. Cache Eviction- Size-based Eviction || Timed Eviction || Reference-based Eviction 
	Because the file might have duplicates, but if there are, then they would all be in close proximity to others of the same value (clusters of up to 50),
	I decided to use Timed Eviction (expireAfterWrite()) which expire entries after the specified duration has passed since the entry was created.
	so, if the duplicate email are close to each other this approach will be satisfied for a solution.

4. Using FasterXML- need to add= ToDo:
	ObjectMapper mapper = new ObjectMapper();
        Map<String, ContactInformation> results = mapper.readValue(personResponse, ContactInformation.class);
	I could not figure out yet how to implement it, I continues to try.

5. if contact have 2 current job, I decided to print just one.

6. If Fullcontact didnt find any information about the person from the email, we will save this email with null value in the cache, so if this email will
	be in the list again we won't need to send another request for nothing.

7. We said it is better not to use regex in the reading words from a file part, but I didn't find another way. something more effective.
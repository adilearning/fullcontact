import Cache.FullContactsContainer;
import FullContactIntegration.FullContactIntegration;
import FullContactIntegration.FullContactConnection;
import Inpout.MailFileReader;
import Outpout.CSVOutputFile;
import lombok.extern.slf4j.Slf4j;

/**
 *  Connect to FullContact and read a list of emails from a file, get the relevant details of the person from FullContact
 *  and write the result to a local Excel/CSV file
 */
@Slf4j
public class Main {

    private static MailFileReader mailFileReader = null;
    private static FullContactIntegration fullContactIntegration = null;
    private static CSVOutputFile csvOutputFile = null;
    private static FullContactsContainer fullContacts = null;


    public Main() {
        mailFileReader = new MailFileReader();
        fullContactIntegration = new FullContactConnection();
        fullContactIntegration.connect();
        csvOutputFile = new CSVOutputFile();
        fullContacts = new FullContactsContainer(fullContactIntegration);
    }

    public static void main (String[] args){
        String mail = null;
        while ((mail = mailFileReader.readLine()) != null){
            System.out.println(mail);

        }

    }

    public void closeAllConnections(){
        mailFileReader.closeFile();
        fullContactIntegration.disconnect();
        csvOutputFile.closeFile();
    }
}

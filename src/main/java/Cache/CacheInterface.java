package Cache;

public interface CacheInterface {

    public Object getEntry(String key);
}

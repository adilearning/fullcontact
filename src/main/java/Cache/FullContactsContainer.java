package Cache;

import FullContactInformation.ContactInfoConvert;
import FullContactInformation.ContactInformation;
import FullContactIntegration.FullContactConnection;
import com.fullcontact.api.libs.fullcontact4j.FullContactException;
import com.fullcontact.api.libs.fullcontact4j.http.person.PersonResponse;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.TimeUnit;

/**
 * A map that hold the email and their relevant ContactInformation object that we get as a response
 * from FullContact
 */
public class FullContactsContainer implements CacheInterface{

    private static final long MAX_SIZE = 100;
    private final LoadingCache<String, ContactInformation> cache;

    public FullContactsContainer(FullContactConnection Connection) {
        cache = CacheBuilder.newBuilder()
                .expireAfterWrite(MAX_SIZE, TimeUnit.MINUTES)
                .build(new CacheLoader<String, ContactInformation>(){
                           @Override
                           public ContactInformation load(String key) throws Exception {
                               return getContactInfo(key,Connection);
                           }
                       }
                );
    }

    @Override
    public ContactInformation getEntry(String key) {
        return  cache.getUnchecked(key);
    }

    private ContactInformation getContactInfo(String email,FullContactConnection Connection) throws FullContactException {
        PersonResponse response = Connection.getResponse(email);
        ContactInfoConvert convert = new ContactInfoConvert();
        return convert.createContactInfoEntity(response);
    }
}

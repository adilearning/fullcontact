package Inpout;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Read from a file line by line, and send email
 * - Assume each email is in a single line and is valid
 */
@Slf4j
public class MailFileReader {

    private static String fileName = "src/Main/resources/file.txt";
    private static BufferedReader br = null;

    public MailFileReader() {
        try {
            br = new BufferedReader(new FileReader(fileName));
        } catch (FileNotFoundException e) {
            log.error("Error reading the file", e);
        }
    }

    public String readLine() {
        String line = null;
        try {
            while ((line = br.readLine()) != null) {
                break;
            }
        } catch (IOException e) {
            log.error("Error reading the file", e);
            try {
                br.close();
            } catch (IOException e1) {
                log.error("Error reading the file", e1);
            }
        }
        return line;
    }

    public void closeFile(){
        try {
            br.close();
        } catch (IOException e) {
            log.error("Error reading the file", e);
        }
    }
}

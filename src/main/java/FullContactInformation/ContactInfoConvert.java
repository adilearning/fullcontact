package FullContactInformation;

import com.fullcontact.api.libs.fullcontact4j.http.person.PersonResponse;
import com.fullcontact.api.libs.fullcontact4j.http.person.model.Demographics;
import com.fullcontact.api.libs.fullcontact4j.http.person.model.Organization;

import java.util.List;

/**
 * Convert the response from FullContact to ContactInformation object
 */
public class ContactInfoConvert {


    public ContactInfoConvert() {
    }

    public ContactInformation createContactInfoEntity(PersonResponse personResponse){
        if(personResponse == null){
            return new ContactInformation();
        }
        com.fullcontact.api.libs.fullcontact4j.http.person.model.ContactInfo contactInfo = personResponse.getContactInfo();
        String name = contactInfo.getFullName();
        Demographics demographics = personResponse.getDemographics();
        String age = demographics.getAge();
        String gender = demographics.getGender();
        String city = demographics.getLocationDeduced().getCity().getName();
        String country = demographics.getLocationDeduced().getCountry().getName();
        String address = city + "," + "," + "," + country;
        List<Organization> organizationList = personResponse.getOrganizations();
        String company = " ";
        for (Organization organization:organizationList){
            if(organization.isCurrent()){
                company = organization.getName();
                break;
            }
        }
        double likelihood =  personResponse.getLikelihood();
        ContactInformation fullContactInfo = new ContactInformation(name,age,gender,company,address,likelihood);
        return fullContactInfo;
    }
}

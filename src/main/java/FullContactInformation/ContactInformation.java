package FullContactInformation;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A POJO class that represents the contact information that return from Fullcontact.
 * It's hold only the information that we need ()
 */
public class ContactInformation {

    private String fullName;
    private String age;
    private String gender;
    private String company;
    private String address;
    private Double likelihood;

    public ContactInformation(){
    }

    public ContactInformation(String fullName, String age, String gender, String company, String address, Double likelihood) {
        this.fullName = fullName;
        this.age = age;
        this.gender = gender;
        this.company = company;
        this.address = address;
        this.likelihood = likelihood;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLikelihood() {
        return likelihood;
    }

    public void setLikelihood(Double likelihood) {
        this.likelihood = likelihood;
    }


    @Override
    public String toString() {
        return  "fullName='" + fullName + '\'' +
                ", age='" + age + '\'' +
                ", gender='" + gender + '\'' +
                ", company='" + company + '\'' +
                ", address='" + address + '\'' +
                ", likelihood=" + likelihood
                ;
    }

}

package FullContactIntegration;

import FullContactInformation.ContactInformation;
import Outpout.CSVOutputFile;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fullcontact.api.libs.fullcontact4j.FullContact;
import com.fullcontact.api.libs.fullcontact4j.FullContactException;
import com.fullcontact.api.libs.fullcontact4j.http.person.PersonRequest;
import com.fullcontact.api.libs.fullcontact4j.http.person.PersonResponse;
import com.fullcontact.api.libs.fullcontact4j.http.person.model.Demographics;
import com.fullcontact.api.libs.fullcontact4j.http.person.model.Organization;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

/**
 * An Integration implementation that connect and read data from FullContact
 */
@Slf4j
public class FullContactConnection implements FullContactIntegration {

    private static FullContact fullContact;
    //private static CSVOutputFile outputCsvFile;
    private static String key = "2447c31b35499847";

    @Override
    public void connect(){
        fullContact = FullContact.withApiKey(key).build();
        //outputCsvFile = new CSVOutputFile();
    }

    @Override
    public PersonResponse getResponse(String email) {
        PersonRequest personRequest = fullContact.buildPersonRequest().email(email).build();
        PersonResponse personResponse = null;
        try {
            personResponse = fullContact.sendRequest(personRequest);
        } catch (FullContactException e) {
            // Contact didn't found- e.getMessage().equals("Searched within last 24 hours. No results found for this Id.")
            log.info("Contact didn't found", e);
        }
//        ObjectMapper mapper = new ObjectMapper();
//        Map<String, ContactInformation> results = mapper.readValue(personResponse, ContactInformation.class);
        return personResponse;
    }
/*
    private ContactInformation createContactInfoEntity(PersonResponse personResponse){
        if(personResponse == null){
            return new ContactInformation();
        }
        com.fullcontact.api.libs.fullcontact4j.http.person.model.ContactInfo contactInfo = personResponse.getContactInfo();
        String name = contactInfo.getFullName();
        Demographics demographics = personResponse.getDemographics();
        String age = demographics.getAge();
        String gender = demographics.getGender();
        String city = demographics.getLocationDeduced().getCity().getName();
        String country = demographics.getLocationDeduced().getCountry().getName();
        String address = city + "," + "," + "," + country;
        List<Organization> organizationList = personResponse.getOrganizations();
        String company = " ";
        for (Organization organization:organizationList){
            if(organization.isCurrent()){
                company = organization.getName();
                break;
            }
        }
        double likelihood =  personResponse.getLikelihood();
        ContactInformation fullContactInfo = new ContactInformation(name,age,gender,company,address,likelihood);
        // Print the return values from fullContact (ContactInformation object) to a CSV file
        //printToCSV(fullContactInfo);
        return fullContactInfo;
    }
*/
    @Override
    public void disconnect(){
        fullContact.shutdown();
        //closeCSVFile();
    }
/*
    private void printToCSV(ContactInformation fullContactInfo){
        outputCsvFile.addContactInfo(fullContactInfo);
    }

    private void closeCSVFile(){
        outputCsvFile.closeFile();
    }
*/
}

package FullContactIntegration;

/**
 * Interface to make integration to FullContact system
 */
public interface FullContactIntegration {

    public void connect();

    public void disconnect();

    public Object getResponse(String key);
}

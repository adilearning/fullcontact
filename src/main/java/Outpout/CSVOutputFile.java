package Outpout;

import FullContactInformation.ContactInformation;
import com.csvreader.CsvWriter;
import lombok.extern.slf4j.Slf4j;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Write the ContactInformation object to local CSVOutputFile file
 */
@Slf4j
public class CSVOutputFile {

    String outputFile = "testCSV12.csv";
    private static CsvWriter csvOutput = null;

    public CSVOutputFile() {
        try {
            csvOutput = new CsvWriter(new FileWriter(outputFile, true), ',');
            csvOutput.write("Name");
            csvOutput.write("Age");
            csvOutput.write("Gender");
            csvOutput.write("Company");
            csvOutput.write("Address (in the format {city},{street and number},{country})");
            csvOutput.write("Probability");
            csvOutput.endRecord();
        } catch (IOException e) {
            csvOutput.close();
            log.error("Error writing to the file", e);
        }
    }

    public void addContactInfo(ContactInformation contactInfo){
        try {
            csvOutput.write(contactInfo.getFullName());
            csvOutput.write(contactInfo.getAge());
            csvOutput.write(contactInfo.getGender());
            csvOutput.write(contactInfo.getCompany());
            csvOutput.write(contactInfo.getAddress());
            csvOutput.write(contactInfo.getLikelihood().toString());
            csvOutput.endRecord();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void closeFile(){
        csvOutput.close();
    }
}
